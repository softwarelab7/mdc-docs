KML and PDF files.
~~~~~~~~~~~~~~~~~~

There are several files that can be saved within this site.

1. KML files.

2. PDF files on some special maps.

.. _savekml-how-to-save-kml-files:

How to Save KML Files
~~~~~~~~~~~~~~~~~~~~~

1. Choose any Layers you want to save. Either Layers and or GeoHazards
   and or Ayala Assests you want to save KML file. You will see a KML
   file beside the zoom button. This file will appear for just few
   seconds, so you will need to look at it and click the file right
   away.

   |KML Files1|

2. When you click the KML file, it will ask you to open or save the
   file.

   |KML Files2|

3. Click **Save File**

   |KML Files3|

4. Click **OK**, to save file.

   |KML Files4|

   |KML Files5|

.. _savekml-how-to-save-pdf-files:

How to save PDF Files
~~~~~~~~~~~~~~~~~~~~~

There are several PDF files that can be found on Ayala Assests that we
can save. Here are the steps on how to do save this.

1. Click on the PDF file that you will see on the map.

   |KML Files6|

   |KML Files7|

2. The PDF file will be open on a new tab or on your PDF file reader,
   make sure that you have a PDF reader to be able to open and save the
   PDF file.

3. When the PDF file loads up, you can now save it using your page
   browser or pdf file reader.

.. |KML Files1| image:: /images/kml.png
.. |KML Files2| image:: /images/kml1.png
.. |KML Files3| image:: /images/kml2.png
.. |KML Files4| image:: /images/kml3.png
.. |KML Files5| image:: /images/kml4.png
.. |KML Files6| image:: /images/kml5.png
.. |KML Files7| image:: /images/kml6.png
