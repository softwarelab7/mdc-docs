How to Save files and Print
~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Click **Print icon** at the lower right corner of the screen.

   |Save and Print1|

   You will see screen like below.

   |Save and Print2|

2. Press **Ctrl P**

   |Save and Print3|

3. Click **Print to File**. You can change the file name by changing
   **Name**, folder it will be at by browsing where in **Save in
   Folder**, Choose what file it will save as, either **PDF file** or
   **Postscript file**.

   |Save and Print4|

4. Click **Print** to save and print the file.

   |Save and Print5|

5. Click on **Print Icon** to go see menu and zoom bar back.

   |Save and Print6|

6. To access the saved file, go to the file path you save the file and
   open it.

   |Save and Print7|

.. |Save and Print1| image:: /images/saveprint.png
.. |Save and Print2| image:: /images/saveprint1.png
.. |Save and Print3| image:: /images/saveprint2.png
.. |Save and Print4| image:: /images/saveprint3.png
.. |Save and Print5| image:: /images/saveprint4.png
.. |Save and Print6| image:: /images/saveprint6.png
.. |Save and Print7| image:: /images/saveprint5.png
