.. _maps-how-to-navigate-maps:

How to Navigate Maps
~~~~~~~~~~~~~~~~~~~~

-  To browse map, long left click then drag the map where you want to
   check.

.. _maps-how-to-change-base-maps:

How to change Base Maps
~~~~~~~~~~~~~~~~~~~~~~~

-  Choose from the drop down of the map you want to check out, located
   at the upper right corner of the screen. Please see picture below.

|Maps1|

|Maps2|

-  The map will load by itself we just need to wait for the map to load
   the new view. You can change base map at any point when you are using
   this system.

|Maps3|

.. _maps-how-to-zoom-in-and-out-on-map:

How to zoom in and out on map
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are 3 ways to zoom in or out the map.

1. **Scroll down** to zoom in and scroll up on the map you want to
   check out.

2. Slide bar.

3. Click **+ or -** above the slide bar.

   |Maps4|

**Maps can only be zoomed in basing on their map detail capacity.**

Below are some samples

-  Here you can see that we have zoom in the map on a view where we can
   still see the data.

|Maps5|

-  Some maps can be zoom in to 100% yet you won't be able to see any
   pertinent data.

|Maps6|

-  Some maps will give you an error message if it is not able to zoom in
   further.

|Maps7|

.. _maps-how-to-rotate-map:

How to Rotate Map
~~~~~~~~~~~~~~~~~

-  To rotate map, click on **ALT + SHIFT**, then drag the map by long
   left click to where you want to rotate the map.

|Maps8|

-  To rotate map back to its original place click the **small blue
   arrow** below the zoom bar. The radial dial for North, South, East
   and West can also be seen below the small blue arrow.

|Maps9|

The radial dial for North, South, East and West can also be seen below
the small blue arrow to show as a reference.

|Maps10|

NOTE: Northing may also be adjusted on Windows by holding Shift+Alt
while clicking and dragging on the mouse. Laptops and tablets with
multi-touch gesture support may also use their corresponding multi-touch
controls to adjust map orientation.

.. _maps-types-of-maps:

Types of Maps
~~~~~~~~~~~~~

There are several types of maps you can choose from that you can check
**Layers**, or check **GeoHazards** and or check **Ayala Assets**.

-  **Open Street Map**.

|Maps11|

-  **Open Cyle Map**.

|Maps12|

-  **Open Transport Map**.

|Maps13|

-  **ArcGIS Online Map**.

|Maps14|

-  **Google Maps RoadMap**.

|Maps15|

-  **Google Maps Satellite**.

|Maps16|

-  **Google Maps Hybrid**.

|Maps17|

-  **Google Maps Terrain**.

|Maps18|

.. |Maps1| image:: /images/maps.png
.. |Maps2| image:: /images/maps11.png
.. |Maps3| image:: /images/maps12.png
.. |Maps4| image:: /images/maps10.png
.. |Maps5| image:: /images/maps13.png
.. |Maps6| image:: /images/maps15.png
.. |Maps7| image:: /images/maps14.png
.. |Maps8| image:: /images/maps9.png
.. |Maps9| image:: /images/maps16.png
.. |Maps10| image:: /images/maps17.png
.. |Maps11| image:: /images/maps1.png
.. |Maps12| image:: /images/maps2.png
.. |Maps13| image:: /images/maps3.png
.. |Maps14| image:: /images/maps4.png
.. |Maps15| image:: /images/maps5.png
.. |Maps16| image:: /images/maps6.png
.. |Maps17| image:: /images/maps7.png
.. |Maps18| image:: /images/maps8.png
