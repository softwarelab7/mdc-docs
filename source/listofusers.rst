List of Users
~~~~~~~~~~~~~

1. Click **Settings**

   |Reports1|

2. Click **Users**, You may click either of the two links for Users.

   |List of Users2|

3. Here you will see the list of users, the access they have via role,
   number of times the user logs in, date the user last sign in on and
   the IP Address the user used.

   |List of Users3|

.. |Reports1| image:: /images/reports12.png
.. |List of Users2| image:: /images/listofusers.png
.. |List of Users3| image:: /images/listofusers1.png
