Site Usage
~~~~~~~~~~

1. Click **Settings**

   |Reports1|

2. Click **Site Usage**

   |Site Usage2|

3. Here you will see all the users who logged in or out in/of the
   system.

   |Site Usage3|

.. |Reports1| image:: /images/reports12.png
.. |Site Usage2| image:: /images/siteusage.png
.. |Site Usage3| image:: /images/siteusage1.png
