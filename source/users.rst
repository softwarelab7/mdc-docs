Password Reset
~~~~~~~~~~~~~~

1. Press **Settings**

   |Users1|

   You will see this dashboard as shown below. The dash will give you
   information on how long you are logged in on the system.

   |Users2|

2. Click **Users**.

   |Users3|

3. Click **Pencil Icon**

   |Users4|

4. Change your own password by filling out **Password** with your new
   password.

   |Users5|

5. Click **Save** to save your new password.

   |Users6|

6. You will be immediately logged out of the system after changing your
   own password. You will see the image below.

   |Log In7|

7. Log in using your new password.

.. |Users1| image:: /images/user.png
.. |Users2| image:: /images/user1.png
.. |Users3| image:: /images/user2.png
.. |Users4| image:: /images/users3.png
.. |Users5| image:: /images/users4.png
.. |Users6| image:: /images/users5.png
.. |Log In7| image:: /images/login.png
