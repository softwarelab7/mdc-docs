
.. _mapdefinition-layers:

Layers
~~~~~~

The Layers dropdown menu contains basic layers, including:

1. Political boundary
2. Topographic layers
3. Socio-Economic layers

.. _mapdefinition-political-bounderies:

Political Boundary
~~~~~~~~~~~~~~~~~~

-  Political boundary

   Polygon data representing administrative boundaries up to the third
   administrative level (Barangay level) for NCR. Data derived from
   OpenStreet Map extracts, with few topological corrections.

.. _mapdefinition-topographic-layer:

Topographic Layers
~~~~~~~~~~~~~~~~~~

-  Topograpic Layers

   Layers showing topographic relief over NCR.

   These include:

   -  Contour – 1-meter interval contour lines depicting lines that
      connect points of equal elevation. Derived from 1-meter resolution
      LiDAR data.

      \*\* Note that due to the volume of contours, grids were used to
      conceal the lines, allowing the user to freely toggle display for
      each tile. To display contours, click on any tile or grid space.
      Click again any visible contour tiles to hide.

      \*\* To display contour elevation for any given contour line,
      hover your mouse pointer over the feature. A label for contour
      elevation will appear on the right, right next to the zoom
      controls and above any loaded legend.

   -  Slope – Slope data derived from 1-meter resolution LiDAR data,
      measured in percent rise. Re-formatted as polygons marking extents
      with slopes conforming to the following ranges.

      +-----------------+
      | Slope Ranges    |
      +=================+
      | 0-3%            |
      +-----------------+
      | 3.1-8%          |
      +-----------------+
      | 8.1-18%         |
      +-----------------+
      | 18.1-30%        |
      +-----------------+
      | 30.1-50%        |
      +-----------------+
      | 50% or higher   |
      +-----------------+

.. _mapdefinition-socio-economic-layers:

Socio-Economic Layers
~~~~~~~~~~~~~~~~~~~~~

-  Layers depicting basic socio-economic information.

   -  Land use – Existing land use data as depicted in the Metro Manila
      Earthquake Impact Reduction Study (MMEIRS), ca. 2004 (2011
      updates?). Data is segregated between NCR Cities and municipality.

      +-------------------------------------------------------+
      | Land Use Classifications Inclusion                    |
      +=======================================================+
      | Commercial                                            |
      +-------------------------------------------------------+
      | Informal Settlements                                  |
      +-------------------------------------------------------+
      | Low-density Residential                               |
      +-------------------------------------------------------+
      | Medium Density Residential                            |
      +-------------------------------------------------------+
      | High Density Residential                              |
      +-------------------------------------------------------+
      | Public open spaces/ Environmentally-sensitive areas   |
      +-------------------------------------------------------+
      | Hospital                                              |
      +-------------------------------------------------------+
      | Educational facilities                                |
      +-------------------------------------------------------+
      | Government offices                                    |
      +-------------------------------------------------------+
      | Military                                              |
      +-------------------------------------------------------+
      | Industrial                                            |
      +-------------------------------------------------------+
      | Transportation systems                                |
      +-------------------------------------------------------+

   -  Population – Polygon data based on the same political boundary
      (see III-1a, Political Boundary). Attributes were obtained from
      the Philippine Statistics Authority (PSA), and are reflective of
      the 2010 national census. Note that figures were not normalized by
      land area; therefore, numbers represent actual count within area
      and not population density.

   -  Roads – Polyline data based on OpenStreet Map extract, with few
      attribute and topological corrections.

.. _mapdefinition-geohazards:

GeoHazards
~~~~~~~~~~

-  The GeoHazards dropdown menu contains geohazard layers from various
   sources designed for quick reference.

   1. Fault lines. Polylines based on regional-scale fault maps from
      PHIVOLCS. Data is divided into:

      -  Active Faults – Traces are certain and are seen to exhibit
         signs of activity.

      -  Trace Approximate – Evidences point to existence of faults, but
         trace cannot be located with absolute certainty.

      -  West Valley Fault – Lines delineating faults over the West
         Valley Fault System running through the eastern part of NCR.

   2. Flood maps. Flood hazard maps from DOST-Project NOAH. Flood levels
      are shown following these ranges below.

      +----------------------+-------------------------------+---------------------+
      | Flood Level Ranges   |
      +======================+===============================+=====================+
      | Red                  | > 1.5 m (Over 4.9 ft)         | Above chest level   |
      +----------------------+-------------------------------+---------------------+
      | Orange               | - 0.5-1.5 m (1.64 – 4.9 ft)   | Above knee level    |
      +----------------------+-------------------------------+---------------------+
      | Yellow               | - < 0.5 m (Below 1.64 ft)     | Below knee level    |
      +----------------------+-------------------------------+---------------------+

      +--------------------------------------------------+------------+
      | Flood Data Hazard are Available on these Areas   |
      +==================================================+============+
      | Bulacan                                          | Davao      |
      +--------------------------------------------------+------------+
      | Cagayan de Oro                                   | Iloilo     |
      +--------------------------------------------------+------------+
      | Calamba                                          | Mandaue    |
      +--------------------------------------------------+------------+
      | Calumpang                                        | Manila     |
      +--------------------------------------------------+------------+
      | Cavite                                           | Marikina   |
      +--------------------------------------------------+------------+
      | Cebu                                             | Pampanga   |
      +--------------------------------------------------+------------+
      | Curtis                                           | Panay      |
      +--------------------------------------------------+------------+

   3. Landslides. Rain-induced landslide hazard maps from DOST-Project
      NOAH.

      +--------------------------------------+---------------------------------------------------+
      | Hazard Levels Description            |
      +======================================+===================================================+
      | Red                                  | No build zone                                     |
      +--------------------------------------+---------------------------------------------------+
      | Orange                               | Build only with slope intervention, protection    |
      |                                      | and continuous monitoring                         |
      +--------------------------------------+---------------------------------------------------+
      | Yellow                               | Build with slope intervention                     |
      +--------------------------------------+---------------------------------------------------+

      \*\* Note that landslide hazard data is only available for the
      provinces of Bulacan and Rizal.

   4. Liquefaction. Liquefaction data from PHIVOLCS.

      +--------------------------+---------------+
      | Marked Areas Depiction   |
      +==========================+===============+
      | High Hazard              | Pale Orange   |
      +--------------------------+---------------+
      | Moderate hazards         | Pale Yellow   |
      +--------------------------+---------------+

   5. Storm Surge. Storm surge hazard maps from DOST-Project NOAH.

      +---------------------------+----------+
      | Hazard Levels Depiction   |
      +===========================+==========+
      | Low                       | Yellow   |
      +---------------------------+----------+
      | Medium                    | Orange   |
      +---------------------------+----------+
      | High                      | Red      |
      +---------------------------+----------+

      \*\* Note that these hazard levels follow the same configuration
      as flood hazard maps (see Flood maps).

      -  Storm surge hazard maps are segregated into four (4) storm
         surge advisory (SSA) levels.

      +-----------------------------------------+------------------------------+
      | Storm Surge Advisory Level Definition   |
      +=========================================+==============================+
      | SSA1                                    | 2-meter storm surge height   |
      +-----------------------------------------+------------------------------+
      | SSA2                                    | 3-meter storm surge height   |
      +-----------------------------------------+------------------------------+
      | SSA3                                    | 4-meter storm surge height   |
      +-----------------------------------------+------------------------------+
      | SSA4                                    | 5-meter storm surge height   |
      +-----------------------------------------+------------------------------+

      \*\* Note that storm surge hazard maps are only available for
      Cebu, Mandaue and Metro Manila.

   6. MGB Hazard Maps. This multi-hazard layer is composed of a
      combination of rain-induced landslide and flood maps. This layer
      is only available for Metro Manila.

.. _mapdefinition-ayala-assets:

Ayala Assets
~~~~~~~~~~~~

-  The Ayala Assets dropdown menu contains layers from various CAD data.

+------------------------------+
| Layer Classification Group   |
+==============================+
| Residential                  |
+------------------------------+
| Commercial                   |
+------------------------------+
| Infrastructure               |
+------------------------------+
| Uncategorized                |
+------------------------------+

+-----------------------+--------------------------------------------------------------------------------+
| Items in each         |
| Category              |
+=======================+================================================================================+
| NCR                   | Ayala assets found in Metro Manila.                                            |
+-----------------------+--------------------------------------------------------------------------------+
| North Luzon           | Ayala assets found north of Metro Manila.                                      |
+-----------------------+--------------------------------------------------------------------------------+
| South Luzon           | Ayala assets found south of Metro Manila but still within the Luzon island     |
|                       | group.                                                                         |
+-----------------------+--------------------------------------------------------------------------------+
| Vis-Min               | Ayala assets in Visayas and Mindanao.                                          |
+-----------------------+--------------------------------------------------------------------------------+


