.. MDC Web GIS documentation master file, created by
   sphinx-quickstart on Wed Apr 13 13:28:28 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MDC Web GIS Docs
===========================

This is the documentation on how to manage the site MDC Web GIS.

-  :doc:`Getting Started <login>`

   -  :ref:`Logging In <login-logging-in>`

-  :doc:`Types of User Role Access <typeofusers>`

   -  :ref:`Basic Users <typeofusers-basic-users>`

   -  :ref:`Privilege Users <typeofusers-privilege-users>`

   -  :ref:`Admin Users <typeofusers-admin-users>`

-  :doc:`Definition of Terms <mapdefinition>`

   -  :ref:`Layers <mapdefinition-layers>`

   -  :ref:`Political Boundary <mapdefinition-political-bounderies>`

   -  :ref:`Topographic Layers <mapdefinition-topographic-layer>`

   -  :ref:`Socio-Economic Layers <mapdefinition-socio-economic-layers>`

   -  :ref:`GeoHazards <mapdefinition-geohazards>`

   -  :ref:`Ayala Assets <mapdefinition-ayala-assets>`

-  :doc:`Maps <maps>`

   -  :ref:`How to Navigate Maps <maps-how-to-navigate-maps>`

   -  :ref:`How to Change Base Maps <maps-how-to-change-base-maps>`

   -  :ref:`How to Zoom in and out on Map <maps-how-to-zoom-in-and-out-on-map>`

   -  :ref:`How to Rotate Maps <maps-how-to-rotate-map>`

   -  :ref:`Types of Maps <maps-types-of-maps>`

-  :doc:`Special Maps Showing Additional Data <specialmaps>`

   - :ref:`Maps Showing Area and Perimeter <specialmaps-maps-showing-area-and-perimeter>`

   -  :ref:`Maps with PDF <specialmaps-maps-with-pdf>`

   - :ref:`Maps with 2 or more KML files <specialmaps-maps-with-2-or-more-kml-files>`

-  :doc:`Layers <layers>`

   -  :ref:`Political Layer <layers-political-layer>`

   -  :ref:`Topographic Layers <layers-topographic-layer>`

      - :ref:`Topographic Contour Layer <layers-topographic-contour-layer>`

      -  :ref:`Topographic Slope Layer <layers-topographic-slope-layer>`

   -  :ref:`Socio-Economic <layers-socio-economic-layer>`

      - :ref:`Socio-Economic Land Use Layer <layers-socio-economic-land-use-layer>`

      - :ref:`Socio-Economic Population Layer <layers-socio-economic-population-layer>`

      - :ref:`Socio-Economic Roads Layer <layers-socio-economic-roads-layer>`

-  :doc:`GeoHazards <geohazards>`

   -  :ref:`GeoHazards Fault lines <geohazards-faultlines>`

      -  :ref:`GeoHazards Active Faults <geohazards-active-faults>`

      -  :ref:`GeoHazards Trace Approximate <geohazards-trace-approximate>`

      -  :ref:`GeoHazards West Valley Fault <geohazards-west-valley-fault>`

   -  :ref:`GeoHazards Floodmaps <geohazards-floodmap>`

   -  :ref:`GeoHazards Landslide Maps <geohazards-landslides>`

   -  :ref:`GeoHazards Liquefaction Maps <geohazards-liquefaction>`

   -  :ref:`GeoHazards Storm Surge Maps <geohazards-stormsurge1>`

   -  :ref:`GeoHazards MGB Hazard Maps <geohazards-mgbhazardmaps>`

-  :doc:`Ayala Assets <ayalaassests>`

   -  :ref:`Ayala Assets Residential <ayalaassests-residential>`

   -  :ref:`Ayala Assets Commercial <ayalaassests-commercial>`

   -  :ref:`Ayala Assets Infrastructure <ayalaassests-infrastructure>`

   -  :ref:`Ayala Assets Uncategorized <ayalaassests-uncategorized>`

-  :doc:`Length and Area Measurements <drawandarea>`

   -  :ref:`Measuring Length <drawandarea-measuring-length>`

   -  :ref:`Measuring Area <drawandarea-measuring-area>`

-  :doc:`Reports <reports>`

   -  :ref:`Creating A Report <reports-creating-a-report>`

   -  :ref:`Archiving Reports <reports-archiving-reports>`

   -  :ref:`View All Reports <reports-view-all-reports>`

   -  :ref:`View Active Reports <reports-view-active-reports>`

   -  :ref:`Viewing A Report <reports-viewing-a-report>`

   -  :ref:`Viewing Archived Report <reports-viewing-archived-report>`

-  :doc:`Password Reset <users>`

-  :doc:`How to Save and Print files <saveprint>`

-  :doc:`How to Save KML and PDF Files <savekml>`

   -  :ref:`How to Save KML Files <savekml-how-to-save-kml-files>`

   -  :ref:`How to Save PDF Files <savekml-how-to-save-pdf-files>`

-  :doc:`List of Users <listofusers>`

-  :doc:`Site Usage <siteusage>`

-  :doc:`Manage Users <manageusers>`

   -  :ref:`Add a New User <manageusers-add-a-new-user>`

   - :ref:`Changing User Role Access <manageusers-changing-user-role-access>`

   -  :ref:`Delete A User <manageusers-delete-a-user>`

   - :ref:`Password Reset for Other Users <manageusers-password-reset-for-other-users>`
   
-  :doc:`Updating Assets <updating-assets>`


.. toctree::
   :hidden:

   login
   typeofusers
   mapdefinition
   maps
   specialmaps
   layers
   geohazards
   ayalaassests
   drawandarea
   reports
   users
   saveprint
   savekml
   listofusers
   siteusage
   manageusers


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

