.. _updating-assets-setting-up:

Setting up
==========

1. Download WinSCP, PuTTY
2. Get Key For Authenticating with Server to input to WinSCP and PuTTY

.. _updating-assets-go-to-directory-of-app-server:

Go to directory of app server
=============================

1. Get into the server
2. Go to directory of app server:

   ::

       cd ~/mdcorp-docker-compose

.. _updating-assets-go-to-web-gis-container:

Go to web GIS container
=======================

1. :ref:`Go to directory of app server <updating-assets-go-to-directory-of-app-server>`
2. Get server container name currently running web gis:

   ::

       source scripts/helpers.sh && production-env --no-input
       compose ps web

   What you need is something like: ``mdcorpdockercompose_web_1``.

3. Get into the server container (w/o the ``<`` and ``>``:

   ::

       with-docker-env docker exec -it <container_server_name> bash

.. _updating-assets-go-to-rails-console:

Go to Rails Console
===================

1. :ref:`Go to web GIS container <updating-assets-go-to-web-gis-container>`
2. Open up rails console:

   ::

       cd /app
       source /scripts/helpers.sh && rails console

3. Here you will be able to manipulate data.

.. _updating-assets-retrieving-category:

Retrieving Category
===================

1. :ref:`Get into the Rails Console. <updating-assets-go-to-rails-console>`
2. Find by name

   ::

       category = Category.find_by!(name: "Name you want to find")

3. Find by name under a specific category

   ::

       parent_category = Category.find_by!(name: "Name of parent")
       category = Category.find_by!(name: "Name you want to find", parent: parent_category)

Creating a Category
===================

1. :ref:`Get into the Rails Console. <updating-assets-go-to-rails-console>`
2. Create category

   ::

       category = Category.create!(parent: parentCategory, name: "Name of new category", visible: true)

Updating a Category
===================

1. :ref:`Retrieve Category <updating-assets-retrieving-category>`
2. Update details

   ::

       category.name = "new name"
       category.save!

Deleting a Category
===================

1. :ref:`Retrieve Category <updating-assets-retrieving-category>`
2. Delete

   ::

       category.delete

Creating new Asset
==================

1. Upload kml files you need and get into the Rails Console. See
  :ref:`Setting up <updating-assets-setting-up>` and :ref:`Get into the Rails Console. <updating-assets-go-to-rails-console>`
2. Find category you want to put the layer and kmls into. See
  :ref:`Retrieving Category <updating-assets-retrieving-category>`.
3. Create the asset (no kml yet). Depending on if you want to display
   kml features:

   ::

       layer = Layer.create!(category: category, name: "Name of new asset", 
           show_kml_description: true, show_feature_area_and_perimeter: true, toggleable: false)

   You can set these fields as true/false to control behavior:

   1. show\_kml\_description - whether or not GIS will show descriptions
      on KML (attributes)
   2. show\_feature\_area\_and\_perimeter - whether or not GIS will try
      to compute area/perimeter (required to be polygons)
   3. toggleable - whether or not GIS will display data like the current
      Contour Slopes.
   4. visible - whether or not GIS will display layer

.. _updating-assets-retrieving-asset:

Retrieving Asset
================

1. :ref:`Get into the Rails Console. <updating-assets-go-to-rails-console>`
2. Find category where the asset will be added. See `Retrieve
   Category <#retrieving-category>`
3. Find asset

   ::

       layer = Layer.find_by!(name: "Name of asset you want to retrieve", category: category)

Updating an Asset
=================

1. :ref:`Find specific asset <updating-assets-retrieving-asset>`
2. Update details

   ::

       layer.name = "new name"
       layer.show_kml_description = true
       layer.save!

Adding a new KML
================

1. Upload KML files via WinSCP using the Key. Please upload to
   ``/tmp/``.
2. Copy asset files to asset directory:

   ::

       sudo cp /tmp/filename.kml ~/mdcorp-docker-compose/systemassetsdata/

3. Go to web GIS container (Go to web GIS container)
4. Confirm files are available on ``/var/www/public/system``:

   ::

       ls -al /var/www/public/system

   You should see the filenames of the uploaded files that you need to
   update.

5. Find asset needed where you want to upload the KML files (Retrieving
   Asset)
6. Add the KML files:

   ::

       file = File.open('/var/www/public/system/filename.kml')
       kml_file = KmlFile.new(layer: layer, attachment: file)
       file.close
       kml_file.save!

Deleting a KML
==============

1. :ref:`Find specific asset <updating-assets-retrieving-asset>`
2. Find specific KML

   ::

       KmlFile.where(layer: layer)[0] # first kml
       KmlFile.where(layer: layer)[1] # second kml
       KmlFile.where(layer: layer)[2] # third kml
       KmlFile.where(layer: layer)[3] # fourth kml
       KmlFile.where(layer: layer)[1].delete # delete second kml

.. _updating-assets-adding-geotechnical-report:

Adding a new Geotechnical Report
=================================

1. Upload Geotechnical Report PDF file via WinSCP using the Key. Please upload to
   ``/tmp/``.
2. Copy asset files to asset directory:

   ::

       sudo cp /tmp/filename.pdf ~/mdcorp-docker-compose/systemassetsdata/

3. Go to web GIS container (Go to web GIS container)
4. Confirm files are available on ``/var/www/public/system``:

   ::

       ls -al /var/www/public/system

   You should see the filenames of the uploaded file that you need to
   update.

5. Find asset needed where you want to upload the pdf file (Retrieving
   Asset)
6. Add the KML files:

   ::

       file = File.open('/var/www/public/system/filename.pdf')
       kml_file = GeotechnicalReport.new(layer: layer, attachment: file)
       file.close
       kml_file.save!

.. _updating-assets-deleting-geotechnical-report:

Deleting Geotechnical Report
===============================

1. :ref:`Find specific asset <updating-assets-retrieving-asset>`
2. Find specific Geotechnical Report

   ::

       GeotechnicalReport.where(layer: layer)[1].delete # delete geotechnical report
