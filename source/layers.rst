Layers
~~~~~~

1. Click **Menu**

   |Layers 1|

2. Click **Layers**

   |Layers 2|

3. Here you will see all the type of **Layers**

   |Layers 3|

-  Political Layer sample.

   |Layers 4|

   |Layers 5|

-  Topographic Contour Layer sample.

   |Layers 6|

   |Layers 7|

-  Topographic Slope Layer sample.

   |Layers 8|

-  Topographic Contour and Slope Layer sample.

   |Layers 9|

-  Socio-Economic Land Use Layer sample.

   |Layers 10|

-  Socio-Economic Population Layer sample.

   |Layers 11|

-  Socio-Economic Roads Layer sample.

   |Layers 12|

-  Socio-Economic Land Use, Population and Roads Layer sample.

   |Layers 13|

.. _layers-political-layer:

Political Layer
~~~~~~~~~~~~~~~

Political boundary - Polygon data representing administrative boundaries
up to the third administrative level (Barangay level) for NCR. Data
derived from OpenStreet Map extracts, with few topological corrections.

-  Click the **Location/s** you want to see the political layer.

   |Layers 14|

   Here you will see the **Political layer** area of the location you
   choose.

   |Layers 15|

   Hover your mouse on the location on the map to see details, like
   shown on image below.

   |Layers 16|

.. _layers-topographic-layer:

Topographic Layer
~~~~~~~~~~~~~~~~~

-  To see both **Topographic Contour and Slope Layers**, See steps on
   **Topographic Contour Layer and Topographic Slope Layer**. Click
   them both to see both Contour and Slope layers.

   |Layers 26|

.. _layers-topographic-contour-layer:

Topographic Contour Layer
~~~~~~~~~~~~~~~~~~~~~~~~~

Topograpic Layers

-  Layers showing topographic relief over NCR.

-  Contour – 1-meter interval contour lines depicting lines that connect
   points of equal elevation. Derived from 1-meter resolution LiDAR
   data.

   \*\* Note that due to the volume of contours, grids were used to
   conceal the lines, allowing the user to freely toggle display for
   each tile. To display contours, click on any tile or grid space.
   Click again any visible contour tiles to hide.

-  To see **Topographic Contour Layer**, Click on **Contour**.

   |Layers 17|

   Click the **Location/s** you want to see the topographic contour
   layer.

   |Layers 18|

   Here you will see the **Topographic Contour Layer** area of the
   location you choose. Click the location on the map you want to see
   the **Contour Elevation** . It will be shown beside the zoom bar as
   seen on the image below.

   |Layers 19|

   Note: If you will see a **Tiled Map**

   |Layers 20|

   Click on each tile seen on the map for the contour to load up.

   |Layers 21|

   Hover mouse on the location on the map you want to see the **Contour
   Elevation**, it will be shown beside the zoom bar as seen on the
   image below.

   |Layers 22|

.. _layers-topographic-slope-layer:

Topographic Slope Layer
~~~~~~~~~~~~~~~~~~~~~~~

-  Topograpic Layers

-  Layers showing topographic relief over NCR.

   Slope – Slope data derived from 1-meter resolution LiDAR data,
   measured in percent rise. Re-formatted as polygons marking extents
   with slopes conforming to the following ranges.

   +-----------------+
   | Slope Ranges    |
   +=================+
   | 0-3%            |
   +-----------------+
   | 3.1-8%          |
   +-----------------+
   | 8.1-18%         |
   +-----------------+
   | 18.1-30%        |
   +-----------------+
   | 30.1-50%        |
   +-----------------+
   | 50% or higher   |
   +-----------------+

-  To see **Topographic Slope Layer**, Click on **Contour**.

   |Layers 23|

   Click the **Location/s** you want to see the topographic slope
   layer.

   |Layers 24|

   Here you will see the **Topographic Slope Layer** area of the
   location you choose. At the right side you will see the slope
   percentage rise as well.

   |Layers 25|

.. _layers-socio-economic-layer:

Socio-Economic Layer
~~~~~~~~~~~~~~~~~~~~

-  To be able to use **Land Use, Population and Roads Layers** all at
   the same time, do the steps for Socio-Economic Land Use Layer,
   Population and Roads. All the legends for the layers will be shown at
   the right side.

   |Layers 36|

.. _layers-socio-economic-land-use-layer:

Socio-Economic Land Use Layer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Layers depicting basic socio-economic information.

-  Land use – Existing land use data as depicted in the Metro Manila
   Earthquake Impact Reduction Study (MMEIRS), ca. 2004 (2011 updates?).
   Data is segregated between NCR Cities and municipality.

   +-------------------------------------------------------+
   | Land Use Classifications Inclusion                    |
   +=======================================================+
   | Commercial                                            |
   +-------------------------------------------------------+
   | Informal Settlements                                  |
   +-------------------------------------------------------+
   | Low-density Residential                               |
   +-------------------------------------------------------+
   | Medium Density Residential                            |
   +-------------------------------------------------------+
   | High Density Residential                              |
   +-------------------------------------------------------+
   | Public open spaces/ Environmentally-sensitive areas   |
   +-------------------------------------------------------+
   | Hospital                                              |
   +-------------------------------------------------------+
   | Educational facilities                                |
   +-------------------------------------------------------+
   | Government offices                                    |
   +-------------------------------------------------------+
   | Military                                              |
   +-------------------------------------------------------+
   | Industrial                                            |
   +-------------------------------------------------------+
   | Transportation systems                                |
   +-------------------------------------------------------+

-  Click **Land Use**

   |Layers 27|

-  Click the **Location/s** you want to see the Socio-Economic Land
   Use layer.

   |Layers 28|

   You will see the Land Use layer and at the right side its legends.

   |Layers 29|

.. _layers-socio-economic-population-layer:

Socio-Economic Population Layer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Population – Polygon data based on the same political boundary (see
   III-1a, Political Boundary). Attributes were obtained from the
   Philippine Statistics Authority (PSA), and are reflective of the 2010
   national census. Note that figures were not normalized by land area;
   therefore, numbers represent actual count within area and not
   population density.

-  Click **Population**

   |Layers 30|

-  Click the **Location/s** you want to see the Socio-Economic
   Population layer.

   |Layers 31|

   You will see the Population layer and at the right side its legends.

   |Layers 32|

.. _layers-socio-economic-roads-layer:

Socio-Economic Roads Layer
~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Roads – Polyline data based on OpenStreet Map extract, with few
   attribute and topological corrections.

-  Click **Roads**

   |Layers 33|

-  Click the **Location/s** you want to see the Socio-Economic Roads
   layer.

   |Layers 34|

   You will see the Roads layer and at the right side its legends.

   |Layers 35|

.. |Layers 1| image:: /images/menu.png
.. |Layers 2| image:: /images/layers.png
.. |Layers 3| image:: /images/layers1.png
.. |Layers 4| image:: /images/layers4.png
.. |Layers 5| image:: /images/layers27.png
.. |Layers 6| image:: /images/layers8.png
.. |Layers 7| image:: /images/layers26.png
.. |Layers 8| image:: /images/layers11.png
.. |Layers 9| image:: /images/layers12.png
.. |Layers 10| image:: /images/layers16.png
.. |Layers 11| image:: /images/layers19.png
.. |Layers 12| image:: /images/layers22.png
.. |Layers 13| image:: /images/layers23.png
.. |Layers 14| image:: /images/layers3.png
.. |Layers 15| image:: /images/layers4.png
.. |Layers 16| image:: /images/layers27.png
.. |Layers 17| image:: /images/layers6.png
.. |Layers 18| image:: /images/layers7.png
.. |Layers 19| image:: /images/layers8.png
.. |Layers 20| image:: /images/layers24.png
.. |Layers 21| image:: /images/layers25.png
.. |Layers 22| image:: /images/layers26.png
.. |Layers 23| image:: /images/layers9.png
.. |Layers 24| image:: /images/layers10.png
.. |Layers 25| image:: /images/layers11.png
.. |Layers 26| image:: /images/layers12.png
.. |Layers 27| image:: /images/layers14.png
.. |Layers 28| image:: /images/layers15.png
.. |Layers 29| image:: /images/layers16.png
.. |Layers 30| image:: /images/layers17.png
.. |Layers 31| image:: /images/layers18.png
.. |Layers 32| image:: /images/layers19.png
.. |Layers 33| image:: /images/layers20.png
.. |Layers 34| image:: /images/layers21.png
.. |Layers 35| image:: /images/layers22.png
.. |Layers 36| image:: /images/layers23.png
