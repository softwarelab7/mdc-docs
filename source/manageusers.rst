Manage Users
~~~~~~~~~~~~

1. Click **Settings**

   |Reports1|

2. Click **Users**

   |Manage Users2|

3. You will see **List of Users**

   |Manage Users3|

4. You can search a user via email address or role/access using the
   search option. Fill out **Search box** and click **Refresh**.

   |Manage Users4|

   The Search will show the Role/Access or Email related to the keyword
   entered.

   |Manage Users5|

5. You can opt to add a new user, change roles (Access), delete a user,
   do password reset for other users and own password reset.

.. _manageusers-add-a-new-user:

Add a New User
~~~~~~~~~~~~~~

1. Click **Add new** tab

   |Manage Users6|

2. Fill out form, put in unique email address, password and role (Basic
   or Privileged or Admin).

   |Manage Users7|

3. Click **Save**, to save the new username and password.

   |Manage Users8|

   The system will inform you that you have successfully created a new
   user.

   |Manage Users9|

.. _manageusers-changing-user-role-access:

Changing User Role Access
~~~~~~~~~~~~~~~~~~~~~~~~~

1. Click **Pencil Icon** beside the email address of the user you want
   to change role access.

   |Manage Users10|

2. Click the drop down beside **Role** to select what role access you
   want to assign the user. It can be Admin or Privilege or Basic.

   |Manage Users11|

3. Click **Save**, to save changes.

   |Manage Users12|

.. _manageusers-delete-a-user:

Delete A User
~~~~~~~~~~~~~

1. Click **X button** beside the email address you want to delete.

   |Manage Users13|

2. You will go to a page asking you to confirm if you want to delete the
   email address.

3. Click **Yes, I'm sure** to confirm deletion of the user.

   |Manage Users14|

   |Manage Users15|

.. _manageusers-password-reset-for-other-users:

Password Reset for Other Users
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Click **Pencil Icon** beside the email address of the user you want
   to change password.

   |Manage Users16|

2. Enter new password inside the box beside password.

   |Manage Users17|

3. Click **Save** to save the new password.

   |Manage Users18|

   |Manage Users19|

.. |Reports1| image:: /images/reports12.png
.. |Manage Users2| image:: /images/manageusers.png
.. |Manage Users3| image:: /images/manageusers1.png
.. |Manage Users4| image:: /images/manageusers6.png
.. |Manage Users5| image:: /images/manageusers7.png
.. |Manage Users6| image:: /images/manageusers2.png
.. |Manage Users7| image:: /images/manageusers3.png
.. |Manage Users8| image:: /images/manageusers4.png
.. |Manage Users9| image:: /images/manageusers5.png
.. |Manage Users10| image:: /images/manageusers8.png
.. |Manage Users11| image:: /images/manageusers9.png
.. |Manage Users12| image:: /images/manageusers10.png
.. |Manage Users13| image:: /images/manageusers11.png
.. |Manage Users14| image:: /images/manageusers12.png
.. |Manage Users15| image:: /images/manageusers13.png
.. |Manage Users16| image:: /images/manageusers8.png
.. |Manage Users17| image:: /images/manageusers14.png
.. |Manage Users18| image:: /images/manageusers15.png
.. |Manage Users19| image:: /images/manageusers16.png
