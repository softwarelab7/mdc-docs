Types of User Role Access
~~~~~~~~~~~~~~~~~~~~~~~~~

There are several types of user access using MDC Web GIS.

1. Basic Users

2. Privilege Users

3. Admin Users

.. _typeofusers-basic-users:

Basic Users
~~~~~~~~~~~

-  Basic Users can access the following features on the web:

   -  Maps

   -  Layers

   -  GeoHazards

   -  Ayala Assests

   -  Length and Area Measurements

   -  Password Reset

   -  Save and Print Files

   -  Maps Showing Area and Perimeter

   See Images below

   |Type of User Roles/Access1|

   |Type of User Roles/Access2|


.. _typeofusers-privilege-users:

Privilege Users
~~~~~~~~~~~~~~~

-  Privilege Users can access the following features on the web:

   -  Maps

   -  Layers

   -  GeoHazards

   -  Ayala Assests

   -  Length and Area Measurements

   -  Password Reset

   -  Save and Print Files

   -  Maps Showing Area and Perimeter

   -  Save KML and PDF Files

   -  Reports

   -  List of Users

   See Images below

   |Type of User Roles/Access3|

   |Type of User Roles/Access4|


.. _typeofusers-admin-users:

Admin Users
~~~~~~~~~~~

-  Admin Users can access the following features on the web:

   -  Maps

   -  Layers

   -  GeoHazards

   -  Ayala Assests

   -  Length and Area Measurements

   -  Password Reset

   -  Save and Print Files

   -  Maps Showing Area and Perimeter

   -  Save KML and PDF Files

   -  Reports

   -  List of Users

   -  Manage Users

   -  Site Usage

   See Images below

   |Type of User Roles/Access5|

   |Type of User Roles/Access6|

.. |Type of User Roles/Access1| image:: /images/basic.png
.. |Type of User Roles/Access2| image:: /images/basic1.png
.. |Type of User Roles/Access3| image:: /images/admin.png
.. |Type of User Roles/Access4| image:: /images/privilege.png
.. |Type of User Roles/Access5| image:: /images/admin.png
.. |Type of User Roles/Access6| image:: /images/admin1.png
