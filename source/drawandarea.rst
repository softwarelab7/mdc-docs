.. _drawandarea-measuring-length:

Measuring Length
~~~~~~~~~~~~~~~~

1. Click **Draw**

   |Draw 1|

2. Click **Length**,

   |Draw 2|

3. Click on the map where you want to draw the line to be measured. You
   can draw straight lines in any orientations you want. Just remember
   to drag the mouse where you want the line to end. Left click to stop.
   See picture below, the end point is where you can see the blue
   marker. The distance can be seen beside the blue marker, measured in
   meters or kilometers.

   |Draw 3|

.. _drawandarea-measuring-area:

Measuring Area
~~~~~~~~~~~~~~

1. Click **Draw**

   |Draw 4|

2. Click **Area**,

   |Draw 5|

3. Click on the map where you want to draw the line to be measured, you
   can draw straight lines in any orientations you want. Just remember
   to drag the mouse where you want the line to end. Left click to
   complete the line then just continue to draw until you measure the
   area you want to measure. See picture below, the end point is where
   you can see the blue marker.

   |Draw 6|

.. |Draw 1| image:: /images/draw.png
.. |Draw 2| image:: /images/draw1.png
.. |Draw 3| image:: /images/draw2.png
.. |Draw 4| image:: /images/draw.png
.. |Draw 5| image:: /images/draw3.png
.. |Draw 6| image:: /images/draw4.png
