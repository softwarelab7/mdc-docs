Ayala Assets
~~~~~~~~~~~~

1. Click **Menu**

   |Layers1|

2. Click **Ayala Assets**. Here you will see all the Ayala Assets Maps

   |Ayala Assests2|

Here are some samples of Ayala Asset Maps.

-  Residential

|Ayala3|

|Ayala4|

-  Commercial

|Ayala5|

|Ayala6|

-  Infrastructure

|Ayala7|

|Ayala8|

-  Uncategorized

|Ayala9|

|Ayala10|

.. _ayalaassests-residential:

Residential
~~~~~~~~~~~

-  The Ayala Assets dropdown menu contains layers from various CAD data.

+------------------------------+
| Layer Classification Group   |
+==============================+
| Residential                  |
+------------------------------+
| Commercial                   |
+------------------------------+
| Infrastructure               |
+------------------------------+
| Uncategorized                |
+------------------------------+

+-----------------------+--------------------------------------------------------------------------------+
| Items in each         |
| Category              |
+=======================+================================================================================+
| NCR                   | Ayala assets found in Metro Manila.                                            |
+-----------------------+--------------------------------------------------------------------------------+
| North Luzon           | Ayala assets found north of Metro Manila.                                      |
+-----------------------+--------------------------------------------------------------------------------+
| South Luzon           | Ayala assets found south of Metro Manila but still within the Luzon island     |
|                       | group.                                                                         |
+-----------------------+--------------------------------------------------------------------------------+
| Vis-Min               | Ayala assets in Visayas and Mindanao.                                          |
+-----------------------+--------------------------------------------------------------------------------+

-  To see **Residential Areas**, Click **Residential**. Click the
   **Location/s** you want to see the Residential map.

|Ayala11|

To see more details, zoom in by clicking on the map where you want to
check, then scroll to zoom in/out the Residential areas.

|Ayala12|

Note: The green lines are the residential areas. Same instructions on
how to access NCR, North Luzon, South Luzon, Viz-min.

.. _ayalaassests-commercial:

Commercial
~~~~~~~~~~

-  The Ayala Assets dropdown menu contains layers from various CAD data.

+------------------------------+
| Layer Classification Group   |
+==============================+
| Residential                  |
+------------------------------+
| Commercial                   |
+------------------------------+
| Infrastructure               |
+------------------------------+
| Uncategorized                |
+------------------------------+

+-----------------------+--------------------------------------------------------------------------------+
| Items in each         |
| Category              |
+=======================+================================================================================+
| NCR                   | Ayala assets found in Metro Manila.                                            |
+-----------------------+--------------------------------------------------------------------------------+
| North Luzon           | Ayala assets found north of Metro Manila.                                      |
+-----------------------+--------------------------------------------------------------------------------+
| South Luzon           | Ayala assets found south of Metro Manila but still within the Luzon island     |
|                       | group.                                                                         |
+-----------------------+--------------------------------------------------------------------------------+
| Vis-Min               | Ayala assets in Visayas and Mindanao.                                          |
+-----------------------+--------------------------------------------------------------------------------+

-  To see **Commercial Areas**, Click **Commercial**. Click the
   **Location/s** you want to see the Commercial map.

|Ayala13|

To see more details, zoom in by clicking on the map where you want to
check, then scroll to zoom in/out the Commercial areas. Shops and
commercial estabishments are represented by icons.

|Ayala14|

Note: The green lines are the commercial areas. Same instructions on how
to access NCR, North Luzon, South Luzon, Viz-min.

.. _ayalaassests-infrastructure:

Infrastructure
~~~~~~~~~~~~~~

-  The Ayala Assets dropdown menu contains layers from various CAD data.

+------------------------------+
| Layer Classification Group   |
+==============================+
| Residential                  |
+------------------------------+
| Commercial                   |
+------------------------------+
| Infrastructure               |
+------------------------------+
| Uncategorized                |
+------------------------------+

+-----------------------+--------------------------------------------------------------------------------+
| Items in each         |
| Category              |
+=======================+================================================================================+
| NCR                   | Ayala assets found in Metro Manila.                                            |
+-----------------------+--------------------------------------------------------------------------------+
| North Luzon           | Ayala assets found north of Metro Manila.                                      |
+-----------------------+--------------------------------------------------------------------------------+
| South Luzon           | Ayala assets found south of Metro Manila but still within the Luzon island     |
|                       | group.                                                                         |
+-----------------------+--------------------------------------------------------------------------------+
| Vis-Min               | Ayala assets in Visayas and Mindanao.                                          |
+-----------------------+--------------------------------------------------------------------------------+

-  To see **Infrastructure Areas**, Click **Infrastructure**. Click
   the **Location/s** you want to see the Infrastructure map.

|Ayala15|

To see more details, zoom in by clicking on the map where you want to
check, then scroll to zoom in/out the Infrastructure areas.

|Ayala16|

Note: The green lines are the Infrastructure areas. Same instructions on
how to access NCR, North Luzon, South Luzon, Viz-min.

.. _ayalaassests-uncategorized:

Uncategorized
~~~~~~~~~~~~~

-  The Ayala Assets dropdown menu contains layers from various CAD data.

+------------------------------+
| Layer Classification Group   |
+==============================+
| Residential                  |
+------------------------------+
| Commercial                   |
+------------------------------+
| Infrastructure               |
+------------------------------+
| Uncategorized                |
+------------------------------+

+-----------------------+--------------------------------------------------------------------------------+
| Items in each         |
| Category              |
+=======================+================================================================================+
| NCR                   | Ayala assets found in Metro Manila.                                            |
+-----------------------+--------------------------------------------------------------------------------+
| North Luzon           | Ayala assets found north of Metro Manila.                                      |
+-----------------------+--------------------------------------------------------------------------------+
| South Luzon           | Ayala assets found south of Metro Manila but still within the Luzon island     |
|                       | group.                                                                         |
+-----------------------+--------------------------------------------------------------------------------+
| Vis-Min               | Ayala assets in Visayas and Mindanao.                                          |
+-----------------------+--------------------------------------------------------------------------------+

-  To see **Uncategorized Areas**, Click **Uncategorized**. Click
   the **Location/s** you want to see the Uncategorized map.

|Ayala17|

To see more details, zoom in by clicking on the map where you want to
check, then scroll to zoom in/out the Uncategorized areas.

|Ayala18|

Note: The green lines are the Uncategorized areas. Same instructions on
how to access North Luzon, South Luzon, Others, NCR.

.. |Layers1| image:: /images/menu.png
.. |Ayala Assests2| image:: /images/ayala.png
.. |Ayala3| image:: /images/ayala1.png
.. |Ayala4| image:: /images/ayala2.png
.. |Ayala5| image:: /images/ayala3.png
.. |Ayala6| image:: /images/ayala4.png
.. |Ayala7| image:: /images/ayala5.png
.. |Ayala8| image:: /images/ayala6.png
.. |Ayala9| image:: /images/ayala7.png
.. |Ayala10| image:: /images/ayala8.png
.. |Ayala11| image:: /images/ayala1.png
.. |Ayala12| image:: /images/ayala2.png
.. |Ayala13| image:: /images/ayala3.png
.. |Ayala14| image:: /images/ayala4.png
.. |Ayala15| image:: /images/ayala5.png
.. |Ayala16| image:: /images/ayala6.png
.. |Ayala17| image:: /images/ayala7.png
.. |Ayala18| image:: /images/ayala8.png
