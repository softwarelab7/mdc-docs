GeoHazards
~~~~~~~~~~

1. Click **Menu**

   |Layers1|

2. Click **GeoHazard**

   |GeoHazard2|

3. Here you will see all the GeoHazard Maps

   |GeoHazard3|

   -  Here are some samples of GeoHazard Active Fault Line Map. It is
      represented by brown lines.

   |GeoHazards4|

   To see more of the brown line, zoom in by clicking on the map where
   you want, then scroll to check the Active Fault line.

   |GeoHazards5|

   -  Here are some samples of GeoHazard Trace Approximate Line Map. It
      is represented by green lines.

   |GeoHazards6|

   To see more of the green line, zoom in by clicking on the map where
   you want, then scroll to check the Trace Approximate line.

   |GeoHazards7|

   -  Here are some samples of GeoHazard West Valley Fault Map. It is
      represented by red lines.

   |GeoHazards8|

   To see more of the red line, zoom in by clicking on the map where you
   want, then scroll to check the West Valley Fault line.

   |GeoHazards9|

   -  Here are the sample of all Fault lines.

   |GeoHazards10|

   To see more of the line, zoom in by clicking on the map where you
   want, then scroll to check the Fault lines.

   |GeoHazards11|

   -  Here are the samples for Flood lines. Legends for line colors and
      flood levels are located at the right corner of the page.

   |GeoHazards12|

   |GeoHazards13|

   -  Here are the samples for Land slides. Legends for landslides are
      located at the right corner of the page.

   |GeoHazards14|

   |GeoHazards15|

   -  Here are the samples for Liquefaction maps. Legends for
      liquefaction are located at the right corner of the page.

   |GeoHazards16|

   |GeoHazards17|

   -  Here are the samples for Storm Surge maps. Legends for storm surge
      hazard advisory are located at the right corner of the page.

   |GeoHazards18|

   |GeoHazards19|

   -  Here are the samples for MGB Hazard maps. Legends for MGB Hazard
      Maps are located at the right corner of the page.

   |GeoHazards20|

   |GeoHazards21|

.. _geohazards-faultlines:

Fault Lines
~~~~~~~~~~~

-  Fault lines. Polylines based on regional-scale fault maps from
   PHIVOLCS.

-  To see Fault lines, click **Fault Lines**

   |GeoHazards22|

   |GeoHazards23|

.. _geohazards-active-faults:

Active Faults
~~~~~~~~~~~~~

-  Active Faults – Traces are certain and are seen to exhibit signs of
   activity.

-  To see **Active Faults** , click on **Active Faults**.

   NOTE: The brown line are the **Active Faults**.

   |GeoHazards24|

   To see more of the line, zoom in by clicking on the map where you
   want, then scroll to check the Active Fault line.

   |GeoHazards25|

.. _geohazards-trace-approximate:

Trace Approximate
~~~~~~~~~~~~~~~~~

-  Trace Approximate – Evidences point to existence of faults, but trace
   cannot be located with absolute certainty.

-  To see **Trace Approximate** , click on **Trace Approximate**.

   NOTE: The green line is the **Trace Approximate**.

   |GeoHazards26|

   To see more of the line, zoom in by clicking on the map where you
   want, then scroll to check the Trace Approximate line.

   |GeoHazards27|

.. _geohazards-west-valley-fault:

West Valley Fault
~~~~~~~~~~~~~~~~~

-  West Valley Fault – Lines delineating faults over the West Valley
   Fault System running through the eastern part of NCR.

-  To see **West Valley Fault** , click on **West Valley Fault**.

   NOTE: The red line is the **Trace Approximate**.

   |GeoHazards28|

   To see more of the line, zoom in by clicking on the map where you
   want, then scroll to check the West Valley Fault line.

   |GeoHazards29|

.. _geohazards-floodmap:

Flood Map
~~~~~~~~~

-  Flood maps. Flood hazard maps from DOST-Project NOAH. Flood levels
   are shown following these ranges below.

   +----------------------+-------------------------------+---------------------+
   | Flood Level Ranges   |
   +======================+===============================+=====================+
   | Red                  | > 1.5 m (Over 4.9 ft)         | Above chest level   |
   +----------------------+-------------------------------+---------------------+
   | Orange               | - 0.5-1.5 m (1.64 – 4.9 ft)   | Above knee level    |
   +----------------------+-------------------------------+---------------------+
   | Yellow               | - < 0.5 m (Below 1.64 ft)     | Below knee level    |
   +----------------------+-------------------------------+---------------------+

   +--------------------------------------------------+------------+
   | Flood Data Hazard are Available on these Areas   |
   +==================================================+============+
   | Bulacan                                          | Davao      |
   +--------------------------------------------------+------------+
   | Cagayan de Oro                                   | Iloilo     |
   +--------------------------------------------------+------------+
   | Calamba                                          | Mandaue    |
   +--------------------------------------------------+------------+
   | Calumpang                                        | Manila     |
   +--------------------------------------------------+------------+
   | Cavite                                           | Marikina   |
   +--------------------------------------------------+------------+
   | Cebu                                             | Pampanga   |
   +--------------------------------------------------+------------+
   | Curtis                                           | Panay      |
   +--------------------------------------------------+------------+

-  To see **Flood Map**, Click **Flood Map**. Click the
   **Location/s** you want to see the flood map. Legends for flood map
   are located at the right corner of the page.

|GeoHazards30|

To see more details, zoom in by clicking on the map where you want to
check, then scroll to zoom in/out the Flood line. Hover mouse to also
see contour height.

|GeoHazards31|

To see more of the lines, you can change the map. See **Types of
Maps** for more map types.

|GeoHazards32|

.. _geohazards-landslides:

Landslides
~~~~~~~~~~

-  Landslides. Rain-induced landslide hazard maps from DOST-Project
   NOAH.

   +--------------------------------------+---------------------------------------------------+
   | Hazard Levels Description            |
   +======================================+===================================================+
   | Red                                  | No build zone                                     |
   +--------------------------------------+---------------------------------------------------+
   | Orange                               | Build only with slope intervention, protection    |
   |                                      | and continuous monitoring                         |
   +--------------------------------------+---------------------------------------------------+
   | Yellow                               | Build with slope intervention                     |
   +--------------------------------------+---------------------------------------------------+

   \*\* Note that landslide hazard data is only available for the
   provinces of Bulacan and Rizal.

-  To see **Landslide Areas**, Click **Landslide**. Click the
   **Location/s** you want to see the landslide map. Legends for
   Landslides are located at the right corner of the page.

|GeoHazards33|

To see more details, zoom in by clicking on the map where you want to
check, then scroll to zoom in/out the landslide line.

|GeoHazards34|

.. _geohazards-liquefaction:

Liquefaction
~~~~~~~~~~~~

Liquefaction. Liquefaction data from PHIVOLCS.

::

    |     Marked Areas Depiction     |
    |------------------|-------------|
    | High Hazard      | Pale Orange |
    | Moderate hazards | Pale Yellow |

    <br></br>

-  To see **Liquefaction Areas**, Click **Liquefaction**. Click the
   **Location/s** you want to see the liquefaction map. Legends for
   liquefaction are located at the right corner of the page.

|GeoHazards35|

To see more details, zoom in by clicking on the map where you want to
check, then scroll to zoom in/out the liquefaction areas.

|GeoHazards36|

.. _geohazards-stormsurge1:

Storm Surge Maps
~~~~~~~~~~~~~~~~

-  Storm Surge. Storm surge hazard maps from DOST-Project NOAH.

   +---------------------------+----------+
   | Hazard Levels Depiction   |
   +===========================+==========+
   | Low                       | Yellow   |
   +---------------------------+----------+
   | Medium                    | Orange   |
   +---------------------------+----------+
   | High                      | Red      |
   +---------------------------+----------+

   \*\* Note that these hazard levels follow the same configuration as
   flood hazard maps (see Flood maps).

   -  Storm surge hazard maps are segregated into four (4) storm surge
      advisory (SSA) levels.

   +-----------------------------------------+------------------------------+
   | Storm Surge Advisory Level Definition   |
   +=========================================+==============================+
   | SSA1                                    | 2-meter storm surge height   |
   +-----------------------------------------+------------------------------+
   | SSA2                                    | 3-meter storm surge height   |
   +-----------------------------------------+------------------------------+
   | SSA3                                    | 4-meter storm surge height   |
   +-----------------------------------------+------------------------------+
   | SSA4                                    | 5-meter storm surge height   |
   +-----------------------------------------+------------------------------+

   \*\* Note that storm surge hazard maps are only available for Cebu,
   Mandaue and Metro Manila.

-  To see **Storm Surge Map**, Click **Storm Surge Map**. Click the
   **Location/s** you want to see the storm surge map. Legends for
   storm surge hazard advisory are located at the right corner of the
   page.

|GeoHazards37|

To see more details, zoom in by clicking on the map where you want to
check, then scroll to zoom in/out the storm surge line. Hover mouse to
also see contour height.

|GeoHazards38|

Note: Same instructions for Advisory 1, Advisory 2, Advisory 3 and
Advisory 4.

.. _geohazards-mgbhazardmaps:

MGB Hazard Maps
~~~~~~~~~~~~~~~

-  To see **MGB Hazard Map**, Click **MGB Hazard Map**. Click the
   **Location/s** you want to see the MGB hazard map.

|GeoHazards39|

To see more details, zoom in by clicking on the map where you want to
check, then scroll to zoom in/out the MGB hazard map. Legends for MGB
Hazard maps are located at the right corner of the page.

|GeoHazards40|

.. |Layers1| image:: /images/menu.png
.. |GeoHazard2| image:: /images/geohazard.png
.. |GeoHazard3| image:: /images/geohazard1.png
.. |GeoHazards4| image:: /images/geohazard4.png
.. |GeoHazards5| image:: /images/geohazard5.png
.. |GeoHazards6| image:: /images/geohazard6.png
.. |GeoHazards7| image:: /images/geohazard7.png
.. |GeoHazards8| image:: /images/geohazard8.png
.. |GeoHazards9| image:: /images/geohazard9.png
.. |GeoHazards10| image:: /images/geohazard10.png
.. |GeoHazards11| image:: /images/geohazard11.png
.. |GeoHazards12| image:: /images/geohazard13.png
.. |GeoHazards13| image:: /images/geohazard14.png
.. |GeoHazards14| image:: /images/geohazard15.png
.. |GeoHazards15| image:: /images/geohazard16.png
.. |GeoHazards16| image:: /images/geohazard17.png
.. |GeoHazards17| image:: /images/geohazard18.png
.. |GeoHazards18| image:: /images/geohazard19.png
.. |GeoHazards19| image:: /images/geohazard20.png
.. |GeoHazards20| image:: /images/geohazard21.png
.. |GeoHazards21| image:: /images/geohazard22.png
.. |GeoHazards22| image:: /images/geohazard2.png
.. |GeoHazards23| image:: /images/geohazard3.png
.. |GeoHazards24| image:: /images/geohazard4.png
.. |GeoHazards25| image:: /images/geohazard5.png
.. |GeoHazards26| image:: /images/geohazard6.png
.. |GeoHazards27| image:: /images/geohazard7.png
.. |GeoHazards28| image:: /images/geohazard8.png
.. |GeoHazards29| image:: /images/geohazard9.png
.. |GeoHazards30| image:: /images/geohazard12.png
.. |GeoHazards31| image:: /images/geohazard13.png
.. |GeoHazards32| image:: /images/geohazard14.png
.. |GeoHazards33| image:: /images/geohazard15.png
.. |GeoHazards34| image:: /images/geohazard16.png
.. |GeoHazards35| image:: /images/geohazard17.png
.. |GeoHazards36| image:: /images/geohazard18.png
.. |GeoHazards37| image:: /images/geohazard19.png
.. |GeoHazards38| image:: /images/geohazard20.png
.. |GeoHazards39| image:: /images/geohazard21.png
.. |GeoHazards40| image:: /images/geohazard22.png
