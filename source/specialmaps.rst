.. _specialmaps-maps-showing-area-and-perimeter:

Maps Showing Area and Perimeter
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  There are some special maps that can show Area and Perimeter
   measurements of a certain location; inside a polygon. All you have to
   do is to hover the mouse inside the green line to be able to see the
   Area and Perimeter.

   \*\* Note: All access of users have access to these maps.

   |Special Maps1|

.. _specialmaps-maps-with-pdf:

Maps with PDF
~~~~~~~~~~~~~

-  There are some special maps with PDF in it.

   |Special Maps2|

   Some can show area and perimeter measurements with PDF if you hover
   your mouse inside the green line.

   |Special Maps3|

   \*\* Note: Privilege Users and Admin Users have access to these maps.

.. _specialmaps-maps-with-2-or-more-kml-files:

Maps with 2 or more KML files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  There are some maps that have 2 or more KML files that you can
   download.

   |Special Maps4|

   \*\* Note: Privilege Users and Admin Users have access to these maps.

.. |Special Maps1| image:: /images/specialmaps.png
.. |Special Maps2| image:: /images/kml5.png
.. |Special Maps3| image:: /images/specialmaps1.png
.. |Special Maps4| image:: /images/specialmaps2.png
