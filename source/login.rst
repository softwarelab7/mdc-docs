
.. _login-logging-in:

Logging In
~~~~~~~~~~

-  Go to MDC website.

-  Fill out Email address and Password in **Sign In** box.

   |Log In 1|

-  Enter credentials and click **Sign In**.

   |Log In 2|

-  You will see the landing page.

   |Log In 3|

.. |Log In 1| image:: /images/login.png
.. |Log In 2| image:: /images/login1.png
.. |Log In 3| image:: /images/landingpage.png
