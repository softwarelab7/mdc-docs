.. _reports-creating-a-report:

Creating a Report
~~~~~~~~~~~~~~~~~

1. Click **Create Report**

   |Reports1|

2. Click on the map the location you want to report. You will see a form
   that will pop out see image below.

   |Reports2|

3. Fill out the form, choose the **Pin Type**, enter name of the
   report on **Name**, **Latitude and Longitude** are all filled out
   with the coordinates of the location you have clicked, on the map
   earlier at step 2, put in the **Message** you want to put on the
   report and if you want you can also **Attach a file** regarding the
   report using the **Browse button**.

   |Reports3|

4. Click **Report** to save the report.

   |Reports4|

NOTE: You can also create a report by going to **Settings => Reports =>
Click Add New**. Same steps as number 3 above, yet here you will need
to input the latitude and longitude coordinates manually.

.. _reports-archiving-reports:

Archiving Reports
~~~~~~~~~~~~~~~~~

1. Click **Settings**

   |Reports5|

2. Click **Active Reports**

   |Reports6|

3. Click **Pencil Icon** beside the report you want to achrive.

   |Reports7|

4. Click **Archive**, to achrive the report.

   |Reports8|

5. Click **Save**, to save changes.

   |Reports9|

   |Reports10|

6. The other way to achrive report is to view the report first then
   click the achrive button. Please see **Viewing A Report** for
   reference.

   |Reports11|

.. _reports-view-all-reports:

View All Reports
~~~~~~~~~~~~~~~~

There are 2 ways on how you can view all reports.

1. Click **ALL REPORTS**, You will see all reports reported on the map
   via **Colored Pins** and short summary on the lower right corner on
   the map.

   |Reports12|

   |Reports13|

2. Click **Settings => Reports => List of Reports**. Here you will see
   all the reports made on the system.

   |Reports14|

Note: When you zoom in on the map, the reports that you will see, will
only be the reports on the area where you are zooming in on the map.

|Reports15|

Here is an example. There is only one report on this area, reason why
only one report is showing up.

|Reports16|

On the other hand there are 2 reports on the other area, reason why 2
reports are showing up.

|Reports17|

.. _reports-view-active-reports:

View Active Reports
~~~~~~~~~~~~~~~~~~~

There are 2 ways on how you can view active reports.

1. Click **ACTIVE REPORTS**, You will see all reports reported on the
   map via **Colored Pins** and short summary on the lower right
   corner on the map.

   |Reports18|

   |Reports19|

2. Click **Settings => Active Reports => List of Active Reports**.
   Here you will see all active reports existing up to this point of
   time on the system.

   |Reports20|

.. _reports-viewing-a-report:

Viewing A Report
~~~~~~~~~~~~~~~~

There are several ways on how to view a report.

Repeat steps in **View All Reports** or **View Active Reports** .

1. Click **Pin** of the report you want to see.

   |Reports21|

   You will see the report

   |Reports22|

2. Click **Report Title** you want to see under Report Summary.

   |Reports23|

   The map will zoom in to where the report is located then repeat step
   1.

3. Click **Settings => Reports => List of Reports**. Here you will see
   all the reports made on the system.

   |Reports24|

   Click check box of the report you want to see then click **i icon**

   |Reports25|

   You will see the report like this one below.

   |Reports26|

.. _reports-viewing-archived-report:

Viewing Archived Report
~~~~~~~~~~~~~~~~~~~~~~~

1. Click **Settings**

   |Reports27|

2. Click **Archived Reports**

   |Reports28|

3. Click check box of the report you want to see then click **i icon**

   |Reports29|

   |Reports30|

.. |Reports1| image:: /images/reports.png
.. |Reports2| image:: /images/reports1.png
.. |Reports3| image:: /images/reports3.png
.. |Reports4| image:: /images/reports2.png
.. |Reports5| image:: /images/reports12.png
.. |Reports6| image:: /images/reports13.png
.. |Reports7| image:: /images/reports14.png
.. |Reports8| image:: /images/reports15.png
.. |Reports9| image:: /images/reports16.png
.. |Reports10| image:: /images/reports17.png
.. |Reports11| image:: /images/reports20.png
.. |Reports12| image:: /images/reports4.png
.. |Reports13| image:: /images/reports5.png
.. |Reports14| image:: /images/reports6.png
.. |Reports15| image:: /images/reports5.png
.. |Reports16| image:: /images/reports10.png
.. |Reports17| image:: /images/reports11.png
.. |Reports18| image:: /images/reports7.png
.. |Reports19| image:: /images/reports8.png
.. |Reports20| image:: /images/reports9.png
.. |Reports21| image:: /images/reports18.png
.. |Reports22| image:: /images/reports19.png
.. |Reports23| image:: /images/reports21.png
.. |Reports24| image:: /images/reports6.png
.. |Reports25| image:: /images/reports6.png
.. |Reports26| image:: /images/reports23.png
.. |Reports27| image:: /images/reports12.png
.. |Reports28| image:: /images/reports24.png
.. |Reports29| image:: /images/reports25.png
.. |Reports30| image:: /images/reports26.png
