#!/bin/bash

function create-venv() {
	mkvirtualenv mdc-docs
}

function venv-workon() {
	workon mdc-docs
}
